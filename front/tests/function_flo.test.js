console.log('flo');

tr = tableBody.getElementsByTagName("tr");
console.log("Nombre de lignes : " + tr.length)
console.assert(tr.length === 3, "Le nombre de lignes est erroné")

var total = 0
for (i = 0; i < tr.length; i++) {
    total += parseInt(tr[i].getElementsByTagName("td")[4].textContent || tr[i].getElementsByTagName("td")[4].innerHTML);
}
console.log("Somme des prix de vente : " + total)
console.assert(total === 1130, "La somme des prix de ventes est erronée")


createTab().then(() => makeAsserts())
